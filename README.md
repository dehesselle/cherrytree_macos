# CherryTree for macOS

![master branch](https://gitlab.com/dehesselle/cherrytree_macos/badges/master/pipeline.svg?key_text=master)
![latest release](https://gitlab.com/dehesselle/cherrytree_macos/-/badges/release.svg?key_text=latest%20release&key_width=100&value_width=100)

![cherrytree](resources/cherrytree.png)

This project builds a macOS app for [CherryTree](https://www.giuspen.net/cherrytree/).

## Installation

Downloads are available in the [Releases](https://gitlab.com/dehesselle/cherrytree_macos/-/releases) section.  
The app is standalone, relocatable and supports macOS High Sierra up to macOS Sequoia.

## Acknowledgements

Built using other people's work:

- [gtk-osx](https://gitlab.gnome.org/GNOME/gtk-osx) for building GTK with JHBuild.

## License

This work is licensed under [GPL-2.0-or-later](LICENSE).  
CherryTree is licensed under [GPL-3.0-or-later](https://github.com/giuspen/cherrytree/blob/master/license.txt).
