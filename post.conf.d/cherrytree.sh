# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# This file contains everything related to CherryTree.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # no exports desired

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

CHERRYTREE_APP_DIR=$ART_DIR/CherryTree.app
CHERRYTREE_APP_CON_DIR=$CHERRYTREE_APP_DIR/Contents
CHERRYTREE_APP_RES_DIR=$CHERRYTREE_APP_CON_DIR/Resources
CHERRYTREE_APP_BIN_DIR=$CHERRYTREE_APP_RES_DIR/bin
CHERRYTREE_APP_ETC_DIR=$CHERRYTREE_APP_RES_DIR/etc
CHERRYTREE_APP_LIB_DIR=$CHERRYTREE_APP_RES_DIR/lib
CHERRYTREE_APP_FRA_DIR=$CHERRYTREE_APP_CON_DIR/Frameworks
CHERRYTREE_APP_PLIST=$CHERRYTREE_APP_CON_DIR/Info.plist

CHERRYTREE_BUILD=${CHERRYTREE_BUILD:-0}

### functions ##################################################################

function cherrytree_get_version
{
  xmllint \
    --xpath "string(//moduleset/cmake[@id='cherrytree']/branch/@version)" \
    "$SELF_DIR"/modulesets/cherrytree.modules
}

### main #######################################################################

# Nothing here.
