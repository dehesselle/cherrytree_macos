#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2021 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create the application bundle. This also includes patching library link
# paths and all other components that we need to make relocatable.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

SELF_DIR=$(dirname "${BASH_SOURCE[0]}")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#----------------------------------------------------- create application bundle

(
  cd "$SELF_DIR" || exit 1
  export ART_DIR   # is referenced in cherrytree.bundle

  jhb run gtk-mac-bundler resources/cherrytree.bundle
)

lib_change_path @executable_path/../Resources/lib/libuchardet.0.dylib \
  "$CHERRYTREE_APP_CON_DIR"/MacOS/CherryTree

#------------------------------------------------------------- update Info.plist

# enable HiDPI
/usr/libexec/PlistBuddy -c "Add NSHighResolutionCapable bool 'true'" \
  "$CHERRYTREE_APP_PLIST"

# enable dark mode (menubar only, GTK theme is reponsible for the rest)
/usr/libexec/PlistBuddy -c "Add NSRequiresAquaSystemAppearance bool 'false'" \
  "$CHERRYTREE_APP_PLIST"

# update minimum system version according to deployment target
if [ -z "$MACOSX_DEPLOYMENT_TARGET" ]; then
  MACOSX_DEPLOYMENT_TARGET=$SYS_SDK_VER
fi
/usr/libexec/PlistBuddy \
  -c "Set LSMinimumSystemVersion '$MACOSX_DEPLOYMENT_TARGET'" \
  "$CHERRYTREE_APP_PLIST"

# set CherryTree version
/usr/libexec/PlistBuddy -c \
  "Set CFBundleShortVersionString '$(cherrytree_get_version)'" \
  "$CHERRYTREE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Set CFBundleVersion '$CHERRYTREE_BUILD'" \
  "$CHERRYTREE_APP_PLIST"

# set copyright
/usr/libexec/PlistBuddy -c "Set NSHumanReadableCopyright 'Copyright © \
2009-2022 Giuseppe Penone, Evgenii Gurianov'" "$CHERRYTREE_APP_PLIST"

# set app category
/usr/libexec/PlistBuddy -c \
  "Add LSApplicationCategoryType string 'public.app-category.utilities'" \
  "$CHERRYTREE_APP_PLIST"

# set folder access descriptions
/usr/libexec/PlistBuddy -c "Add NSDesktopFolderUsageDescription string \
'CherryTree needs your permission to access the Desktop folder.'" \
  "$CHERRYTREE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDocumentsFolderUsageDescription string \
'CherryTree needs your permission to access the Documents folder.'" \
  "$CHERRYTREE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDownloadsFolderUsageDescription string \
'CherryTree needs your permission to access the Downloads folder.'" \
  "$CHERRYTREE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSRemoveableVolumesUsageDescription string \
'CherryTree needs your permission to access removeable volumes.'" \
  "$CHERRYTREE_APP_PLIST"

# add supported languages
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations array" \
  "$CHERRYTREE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string 'en'" \
  "$CHERRYTREE_APP_PLIST"  # because there is no en.po file
for locale in "$SRC_DIR"/cherrytree_*/po/*.po; do
  /usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string \
'$(basename -s .po $locale)'" "$CHERRYTREE_APP_PLIST"
done

# add some metadata to make CI identifiable
if $CI; then
  for var in PROJECT_NAME PROJECT_URL COMMIT_BRANCH COMMIT_SHA COMMIT_SHORT_SHA\
             JOB_ID JOB_URL JOB_NAME PIPELINE_ID PIPELINE_URL; do
    # use awk to create camel case strings (e.g. PROJECT_NAME to ProjectName)
    /usr/libexec/PlistBuddy -c "Add CI$(\
      echo $var | awk -F _ '{
        for (i=1; i<=NF; i++)
        printf "%s", toupper(substr($i,1,1)) tolower(substr($i,2))
      }'
    ) string $(eval echo \$CI_$var)" "$CHERRYTREE_APP_PLIST"
  done
fi
